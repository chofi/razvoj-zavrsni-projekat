package view.tables.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import api.Api;
import api.Api.DatabaseError;
import entities.Classroom;
import entities.Teacher;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import main.Main;

public class TeachersTableController implements Initializable {
	
	@FXML
	private TableView<Teacher> tableView;
	@FXML
	private TableColumn<Teacher, String> nameColumn;
	@FXML
	private TableColumn<Teacher, String> surnameColumn;
	@FXML
	private TextField NameTextField;
	@FXML
	private TextField SurnameTextField;
	@FXML
	private Label lblError;

	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
            nameColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getName());});
            surnameColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getSurname());});
         
            tableView.setItems(FXCollections.observableArrayList(Api.getTeachers()));
	        tableView.setEditable(true);
	        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
	        surnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

	        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

	}    
	 
	
	public void rerender() {
        tableView.setItems(FXCollections.observableArrayList(Api.getTeachers()));
	}

	
	@FXML
    public void handleNameCellEvent(CellEditEvent edittedCell)
    {
        Teacher teacherSelected =  tableView.getSelectionModel().getSelectedItem();
        Teacher newVal = new Teacher();
        newVal.setName(edittedCell.getNewValue().toString());
        Api.updateTeacher(teacherSelected, newVal);
        rerender();
    }
	
	@FXML
    public void handleSurnameCellEvent(CellEditEvent edittedCell)
    {
        Teacher teacherSelected =  tableView.getSelectionModel().getSelectedItem();
        Teacher newVal = new Teacher();
        newVal.setSurname(edittedCell.getNewValue().toString());
        Api.updateTeacher(teacherSelected, newVal);
        rerender();
    }
	
	@FXML 
	public void handleBtnAddItem() {
		if(NameTextField.getText().length() != 0 && SurnameTextField.getText().length() != 0) {
			String name = NameTextField.getText();
			String surname = SurnameTextField.getText();
			Api.insertTeacher(name,surname);
			rerender();
	        NameTextField.clear();
	        SurnameTextField.clear();
		}
	}
	
	@FXML
	public void handleBtnDeleteItem() {
	ObservableList<Teacher> selectedRows = tableView.getSelectionModel().getSelectedItems();

	       if(selectedRows.size() != 0) {
			   Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati profesora?",
						bool -> {
							if(bool) {
								    for (Teacher row : selectedRows) {
								    	try {DatabaseError e = Api.deleteTeacher(row);
								    	if(e.equals(DatabaseError.OK))
										     rerender();
									    	else
									    	 lblError.setText("Nemoguce izbrisati profesora, koristi se u drugim entitetima u bazi!");	}
								    	catch(Exception e) {
								    		e.printStackTrace();
								    	}
							       
								    }
							}});
		            }
		
	 }
}
