package view.tables.controllers;

import entities.Building;
import entities.Cas;
import api.Api;
import api.Api.DatabaseError;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;

import java.util.Collection;
import java.util.ResourceBundle;
import java.net.URL;

public class BuildingsTableController implements Initializable {
	
	@FXML
	private TableView<Building> tableView;
	@FXML
	private TableColumn<Building, String> nameColumn;
	@FXML
	private TextField NameTextField;
	@FXML
	private Label lblError;

	
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	        nameColumn.setCellValueFactory(new PropertyValueFactory<Building, String>("name"));
	        tableView.setItems(FXCollections.observableArrayList(Api.getBuildings()));
	        tableView.setEditable(true);
	        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
	        
	        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

	}    
	 
	
	public void rerender() {
        tableView.setItems(FXCollections.observableArrayList(Api.getBuildings()));
	}

	
	@FXML
    public void handleNameCellEvent(CellEditEvent edittedCell)
    {
        Building buildingSelected =  tableView.getSelectionModel().getSelectedItem();
        Building newVal = new Building();
        newVal.setName(edittedCell.getNewValue().toString());
        Api.updateBuilding(buildingSelected, newVal);
        rerender();
    }
	
	@FXML 
	public void handleBtnAddItem() {
		Building newBuilding = new Building();
		if(NameTextField.getText().length() != 0) {
			newBuilding.setName(NameTextField.getText()); 
			Api.insertBuilding(newBuilding);
			rerender();
	        NameTextField.clear();
		}
		
	}
	
	@FXML
	public void handleBtnDeleteItem() {
	ObservableList<Building> selectedRows = tableView.getSelectionModel().getSelectedItems();

	       if(selectedRows.size() != 0) {
			   Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati zgradu?",
						bool -> {
							if(bool) {
								    for (Building building : selectedRows) {
								    	try {DatabaseError e = Api.deleteBuilding(building);
								    	if(e.equals(DatabaseError.OK))
										     rerender();
									    	else
									    	 lblError.setText("Nemoguce izbrisati zgradu, koristi se u drugim entitetima u bazi!");	}
								    	catch(Exception e) {
								    		e.printStackTrace();
								    	}
							       
								    }
							}});
		            }
		
	
		

    }
	

}
