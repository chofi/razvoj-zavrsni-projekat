package view.tables.controllers;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

import api.Api;
import entities.Building;
import entities.Cas;
import entities.Reservation;
import entities.Cas.Day;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;

import javafx.stage.Modality;
import javafx.stage.Stage;
import view.dialogs.controllers.EditCasController;
import view.dialogs.controllers.EditReservationController;
import main.Main;


public class CasoviTableController implements Initializable {
	@FXML
	private TableView<Cas> tableView;
    @FXML
	private TableColumn<Cas, String> groupColumn;
	@FXML
	private TableColumn<Cas, String> classroomColumn;
	@FXML
	private TableColumn<Cas, String> dayColumn;
	@FXML
	private TableColumn<Cas, String> startHourColumn;
	@FXML
	private TableColumn<Cas, String> durationColumn;
	@FXML
	private Button btnAddItem;
	@FXML
	private Button btnDeleteItem;

	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
            groupColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getGroup().getName());});
	        classroomColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getClassroom().getName());});
	        dayColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getDay().toString());});
	        startHourColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(new Integer(cellData.getValue().getStartHour()).toString());});
	        durationColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(new Integer(cellData.getValue().getDuration()).toString());});

	        tableView.setItems(FXCollections.observableArrayList(Api.getCasovi()));
	        tableView.setEditable(true);
	        
            tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            
        	
	       
	    }    
	 
		@FXML
	    public void handleEdit(CellEditEvent edittedCell){
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)tableView.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editCas.fxml"));
		        Parent root = loader.load();
		   	    EditCasController editCasController = loader.getController();
		        editCasController.setCas(tableView.getSelectionModel().getSelectedItem());
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Uredi cas");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
	    }
		
		@FXML 
		public void handleBtnAddItem() {
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)btnAddItem.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editCas.fxml"));
		        Parent root = loader.load();
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Dodaj cas");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
		}
		
		private void rerender() {
	        tableView.setItems(FXCollections.observableArrayList(Api.getCasovi()));
		}
	
	@FXML
	public void handleBtnDeleteItem() {
		 ObservableList<Cas> selectedRows = tableView.getSelectionModel().getSelectedItems();
		 
		 if(selectedRows.size() != 0) {
			    Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati cas?",
						bool -> {
							if(bool) {

								    for (Cas cas : selectedRows) {
							            Api.deleteCas(cas);
							            rerender();
								    }
							}});
		 }

 }

     
	
}
