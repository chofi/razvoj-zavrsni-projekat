package view.tables.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import api.Api;
import api.Api.DatabaseError;
import entities.Classroom;
import entities.Building;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import view.dialogs.controllers.EditClassroomController;

public class ClassroomsTableController implements Initializable {
	@FXML
	private TableView<Classroom> tableView;
    @FXML
	private TableColumn<Classroom, String> nameColumn;
	@FXML
	private TableColumn<Classroom, String> buildingColumn;
    @FXML
	private Button btnAddItem;
	@FXML
	private Button btnDeleteItem;
	@FXML
	private Label lblError;

	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
	        nameColumn.setCellValueFactory(new PropertyValueFactory<Classroom, String>("name"));
	        buildingColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getBuilding().getName());});
	      
	        tableView.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
	        tableView.setEditable(true);
	        
            tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            
     }    
	 
		@FXML
	    public void handleEdit(CellEditEvent edittedCell){
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)tableView.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editClassroom.fxml"));
		        Parent root = loader.load();
		   	    EditClassroomController editClassroomController = loader.getController();
		        editClassroomController.setClassroom(tableView.getSelectionModel().getSelectedItem());
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Uredi ucionicu");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
	    }
		
		@FXML 
		public void handleBtnAddItem() {
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)btnAddItem.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editClassroom.fxml"));
		        Parent root = loader.load();
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Dodaj ucionicu");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
		}
		
		private void rerender() {
	        tableView.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
		}
	
		@FXML
		public void handleBtnDeleteItem() {
		ObservableList<Classroom> selectedRows = tableView.getSelectionModel().getSelectedItems();

		       if(selectedRows.size() != 0) {
				   Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati ucionicu?",
							bool -> {
								if(bool) {
									    for (Classroom row : selectedRows) {
									    	System.out.println(row);
									    	try {DatabaseError e = Api.deleteClassroom(row);
									    	if(e.equals(DatabaseError.OK))
											     rerender();
										    	else
										    	 lblError.setText("Nemoguce izbrisati ucionicu, koristi se u drugim entitetima u bazi!");	}
									    	catch(Exception e) {
									    		e.printStackTrace();
									    	}
								       
									    }
								}});
			            }
			
		 }
}
