package view.dialogs.controllers;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.time.temporal.ChronoField;

import api.Api;
import entities.*;
import view.controllers.*;
import main.Main;;

public class ConfirmDialogController {

	private Main.BoolLambda lambda;

	public Main.BoolLambda getLambda() {
		return lambda;
	}

	public void setLambda(Main.BoolLambda lambda) {
		this.lambda = lambda;
	}
	private String promptText;

	@FXML
	public Label lblText;
	@FXML
	public Button btnConfirm;
	@FXML
	public Button btnCancel;

	@FXML
	private void handleButtonConfirm(ActionEvent e) {
		System.out.println("Potvrden");
		lambda.func(true);
		Window window = btnConfirm.getScene().getWindow();
		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@FXML
	private void handleButtonCancel(ActionEvent e) {
		lambda.func(false);

		Window window = btnConfirm.getScene().getWindow();
		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		
	}

	public String getPromptText() {
		return promptText;
	}
	public void setPromptText(String promptText) {
		this.promptText = promptText;
		lblText.setText(promptText);
	}
}
