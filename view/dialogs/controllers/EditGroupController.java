package view.dialogs.controllers;

import api.Api;
import entities.Group;
import entities.Group.GroupType;
import entities.Subject;
import entities.Teacher;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class EditGroupController {
	private Group group = null;

	@FXML
	public Button btnConfirm;
	@FXML
	public Label lblError;
	@FXML
	public TextField inputSemester;
	@FXML
	public ChoiceBox<Teacher> dropdownTeachers;
	@FXML
	public ChoiceBox<GroupType> dropdownTypes;
	@FXML
	public ChoiceBox<Subject> dropdownSubjects;

	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
		if(group != null)
			updateInfo();
	}
	private void updateInfo() {
		inputSemester.setText(group.getSemester());
		dropdownSubjects.setItems(FXCollections.observableArrayList(Api.getSubjects()));
		dropdownTeachers.setItems(FXCollections.observableArrayList(Api.getTeachers()));
		dropdownTypes.setItems(Group.getGroupTypes());
		dropdownSubjects.setValue(group.getSubject());
		dropdownTeachers.setValue(group.getTeacher());
		dropdownTypes.setValue(group.getType());
	}
	@FXML
	private void initialize() {
		dropdownSubjects.setItems(FXCollections.observableArrayList(Api.getSubjects()));
		dropdownTeachers.setItems(FXCollections.observableArrayList(Api.getTeachers()));
		dropdownTypes.setItems(Group.getGroupTypes());
	}
	@FXML
	private void handleConfirm(ActionEvent e) {
		Group newVal = new Group();
		if(	
			inputSemester.getText().equals("") ||
            dropdownSubjects.getValue() == null ||
			dropdownTeachers.getValue() == null ||
			dropdownTypes.getValue() == null
			) {
			lblError.setText("Ispunite sva polja!");
			return;
		}
		
         //validity check
         String s = inputSemester.getText();
		 if(!s.contentEquals("I") && !s.contentEquals("II")  && !s.contentEquals("III")   && !s.contentEquals("IV")  && !s.contentEquals("V") 
			&& !s.contentEquals("VI")  && !s.contentEquals("VII")  && !s.contentEquals("VIII") ) {
				lblError.setText("Moguca vrijednost polja semestar je od I do VIII!");
				return;
		 }
	
		
        newVal.setSubject(dropdownSubjects.getValue());
		newVal.setTeacher(dropdownTeachers.getValue());
		newVal.setType(dropdownTypes.getValue());
        newVal.setSemester(inputSemester.getText());

		if(group == null) {
			Api.insertGroup(newVal);
		}else {
            Api.updateGroup(group, newVal);
		}
		
		Stage appStage=(Stage)btnConfirm.getScene().getWindow();
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
	@FXML
	private void handleCancel(ActionEvent e) {
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		
	}
}
