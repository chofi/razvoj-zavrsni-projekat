package view.dialogs.controllers;

import api.Api;
import entities.Classroom;
import entities.Building;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class EditClassroomController {
	private Classroom classroom = null;

	@FXML
	public Button btnConfirm;
	@FXML
	public Label lblError;
    @FXML
	public TextField inputName;
	@FXML
	public ChoiceBox<Building> dropdownBuildings;
	

	public Classroom getClassroom() {
		return classroom;
	}
	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
		if(classroom != null)
			updateInfo();
	}
	private void updateInfo() {
		inputName.setText(classroom.getName());
		dropdownBuildings.setItems(FXCollections.observableArrayList(Api.getBuildings()));
		dropdownBuildings.setValue(classroom.getBuilding());

	}
	@FXML
	private void initialize() {
		dropdownBuildings.setItems(FXCollections.observableArrayList(Api.getBuildings()));
	
	}
	@FXML
	private void handleConfirm(ActionEvent e) {
		Classroom newVal = new Classroom();
		if(	
			inputName.getText().equals("") ||
			dropdownBuildings.getValue() == null
			) {
			lblError.setText("Ispunite sva polja!");
			return;
		}


		newVal.setBuilding(dropdownBuildings.getValue());
		newVal.setName(inputName.getText());

		if(classroom == null) {
			Api.insertClassroom(newVal);
		}else {
            Api.updateClassroom(classroom, newVal);
		}
		
		Stage appStage=(Stage)btnConfirm.getScene().getWindow();
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
	@FXML
	private void handleCancel(ActionEvent e) {
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		
	}
}
