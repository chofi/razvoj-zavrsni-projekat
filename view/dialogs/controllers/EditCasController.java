package view.dialogs.controllers;


import api.Api;
import entities.Classroom;
import entities.Cas;
import entities.Group;
import entities.Cas.Day;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class EditCasController {
	private Cas cas = null;

	@FXML
	public Button btnConfirm;
	@FXML
	public Label lblError;
	@FXML
	public TextField inputStartHour;
	@FXML
	public TextField inputDuration;
	@FXML
	public ChoiceBox<Classroom> dropdownClassrooms;
	@FXML
	public ChoiceBox<Group> dropdownGroups;
	@FXML
	public ChoiceBox<Day> dropdownDays;

	public Cas getCas() {
		return cas;
	}
	public void setCas(Cas cas) {
		this.cas = cas;
		if(cas != null)
			updateInfo();
	}
	private void updateInfo() {
		inputStartHour.setText(Integer.toString(cas.getStartHour()));
		inputDuration.setText(Integer.toString(cas.getDuration()));
		dropdownClassrooms.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
		dropdownClassrooms.setValue(cas.getClassroom());
		dropdownGroups.setItems(FXCollections.observableArrayList(Api.getGroups()));
		dropdownGroups.setValue(cas.getGroup());
		dropdownDays.setItems(Cas.getDays());
		dropdownDays.setValue(cas.getDay());
	}
	@FXML
	private void initialize() {
		dropdownClassrooms.setItems(FXCollections.observableArrayList(Api.getClassrooms()));
		dropdownGroups.setItems(FXCollections.observableArrayList(Api.getGroups()));
		dropdownDays.setItems(Cas.getDays());
	}
	@FXML
	private void handleConfirm(ActionEvent e) {
		Cas newVal = new Cas();
		if(	
			inputStartHour.getText().equals("") ||
			inputDuration.getText().equals("") ||
			dropdownClassrooms.getValue() == null ||
			dropdownGroups.getValue() == null ||
			dropdownDays.getValue() == null
			) {
			lblError.setText("Ispunite sva polja!");
			return;
		}

		try {
			int duration =(new Integer(inputDuration.getText()));
			int startHour = (new Integer(inputStartHour.getText()));
			if(startHour < 8 || startHour > 19) {
				lblError.setText("Moguce je rezervisati samo u terminu 8:00 do 20:00!");
				return;
			}
			if(duration > 11) {
				lblError.setText("Dozvoljeno trajanje je maksimalno 11h!");
				return;
			}
			if(duration < 1) {
				lblError.setText("Minimalno trajanje je 1h!");
				return;
			}
			if(startHour + duration > 20) {
				lblError.setText("Trajanje premasuje 20:00!");
				return;
			}
		}catch(Throwable err) {
			lblError.setText("Dozvoljeni su samo brojevi za polja pocetak i trajanje!");
			return;
		}

		

		newVal.setClassroom(dropdownClassrooms.getValue());
		newVal.setGroup(dropdownGroups.getValue());
		newVal.setDay(dropdownDays.getValue());
        newVal.setDuration(new Integer(inputDuration.getText()));
		newVal.setStartHour(new Integer(inputStartHour.getText()));

		if(cas == null) {
			Api.insertCas(newVal);
			System.out.println("Insert");
		}else {
            Api.updateCas(cas, newVal);
		}
		
		Stage appStage=(Stage)btnConfirm.getScene().getWindow();
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}
	@FXML
	private void handleCancel(ActionEvent e) {
		Window window = btnConfirm.getScene().getWindow();

		window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
		
	}

}
