package view.controllers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

import javax.imageio.ImageIO;

import api.Api;
import entities.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Pair;

public class IzvjestajController {
	Label[][] labels;
	@FXML
	private GridPane gridPredmeti;
	@FXML
	private Pane mainPane;
	@FXML
	private GridPane gridUkupnoSati;
	@FXML
	private Label lblSumP;
	@FXML
	private Button btnSave;
	@FXML
	private Label lblSumV;
	@FXML
	private void handleButtonSave(ActionEvent e) {
		WritableImage img = new WritableImage((int)(mainPane.getWidth()), (int)(mainPane.getHeight()));
				mainPane.snapshot(new SnapshotParameters(), img);
	          FileChooser fileChooser = new FileChooser();

	          //Set extension filter
	          FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
	          fileChooser.getExtensionFilters().add(extFilter);

	          //Show save file dialog
	          File file = fileChooser.showSaveDialog(gridPredmeti.getScene().getWindow());

	          if(file != null){
	              SaveFile(img, file);

	              Window window = btnSave.getScene().getWindow();

	              window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	          }
	}
	private void SaveFile(Image content, File file){
	    try {
	        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(content, null);
	        ImageIO.write(bufferedImage, "png", file);
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }

	}
	public void generate(Teacher teacher, LocalDate date){
		Collection<Pair<LocalDate, Cas>> casovi = Api.getIzvjestajCasovi(teacher, date);
		initLabels();
		int j = 0;
		int sumP = 0;
		int sumV = 0;
		
		for(Pair<LocalDate, Cas> pair : casovi)
		{
			LocalDate d = pair.getKey();
			Cas cas = pair.getValue();

			if(j >= 19)
				break;
			
			labels[0][j].setText(cas.getGroup().getSubject().getName());
			labels[1][j].setText(d.toString());
			labels[2][j].setText(cas.getClassroom().getName() + " " + cas.getStartHour() + ":00h");
			labels[4][j].setText("-");
			labels[5][j].setText("-");
			if(cas.getGroup().getType() == Group.GroupType.PRED) {
				labels[4][j].setText(Integer.toString(cas.getDuration()));
				sumP += cas.getDuration();
			}
			else {
				labels[5][j].setText(Integer.toString(cas.getDuration()));
				sumV += cas.getDuration();
			}
			
			j++;
		}
		lblSumP.setText(Integer.toString(sumP));
		lblSumV.setText(Integer.toString(sumV));
		

	}
	private void initLabels() {
		System.out.println("Poceo");
		labels = new Label[6][19];
		for(int i = 0; i < 6; i++ ) {
			for(int j = 0; j < 19; j++) {
				Label hbox = new Label();
				hbox.setAlignment(Pos.CENTER);
				hbox.setPrefWidth(1000);
				if(i == 2)
					hbox.setAlignment(Pos.CENTER_RIGHT);
				gridPredmeti.add(hbox, i, j);
				labels[i][j] = hbox;
			}
		}
	}

}
