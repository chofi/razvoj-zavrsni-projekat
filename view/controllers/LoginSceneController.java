package view.controllers;


import java.io.IOException;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import api.*;
import entities.User;

public class LoginSceneController {
	private User user = null;
	@FXML
	public Button btnLogin;
	@FXML
	public Button btnLoginGuest;
	@FXML
	public TextField inputUsername;
	@FXML
	public PasswordField inputPassword;
	@FXML
	public Label lblNotify;
	@FXML
	public Button btnRegister;
	
	@FXML
	private void handleGuestLogin(ActionEvent event) 
	{
	    switchToScene("AlternativeStudentScene");
	}
	
	@FXML
	private void handleLogin(ActionEvent event) 
	{
	    String username = inputUsername.getText();
	    String password = inputPassword.getText();
	    
	    user = Api.login(username, password);
	    
	    if(user == null) {
	    	lblNotify.setText("Pogresni podaci!");
	    }else {
	    	if(user.getType() == User.AccountType.Professor) {
	    		switchToScene("AlternativeProfessorScene");
	    	}else if(user.getType() == User.AccountType.Dean) {
	    		switchToScene("AlternativeDeanScene");
	    	}
	    }
	}
	
	@FXML
	private void handleRegistration(ActionEvent event) {
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnRegister.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../RegisterScene.fxml"));
	        Parent root = loader.load();
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Registracija");
	        dialog.show();
		}
		catch(Throwable e) {
			System.out.println(e);}
	}

	private void switchToScene(String sceneName)
	{
	    try {
		    Stage appStage=(Stage)btnLoginGuest.getScene().getWindow();
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../" + sceneName + ".fxml"));
	        Parent root = loader.load();
	        if(sceneName == "AlternativeProfessorScene") {
	        	AlternativeProfessorSceneController controller = (AlternativeProfessorSceneController) loader.getController();
	        	controller.setLoggedInUser(user);
	        }
	        if(sceneName == "AlternativeDeanScene") {
	        	AlternativeDeanSceneController controller = (AlternativeDeanSceneController) loader.getController();
	        	controller.setLoggedInUser(user);
	        }
	        Scene scene=new Scene(root);
	        scene.getStylesheets().add("view/stylesheet.css");
	        appStage.setScene(scene);
	        appStage.show();
	    }catch(IOException e) {
	    	e.printStackTrace();
	    }
	}
}
