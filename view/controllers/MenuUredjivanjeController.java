package view.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MenuUredjivanjeController {
	
	@FXML
	public Button btnZgrade;
	@FXML
	public Button btnCasovi;
	@FXML
	public Button btnUcionice;
	@FXML
	public Button btnGrupe;
	@FXML
	public Button btnRezervacije;
	@FXML
	public Button btnPredmeti;
	@FXML
	public Button btnProfesori;
	@FXML
	public Button btnUsmjerenja;
	
	

	
	
	private void handleAnyButton(Button button,String url, String title) {
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnZgrade.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource(url));
	        Parent root = loader.load();
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle(title);
	        dialog.show();
		}
		catch(Throwable e) {System.out.println(e);}
	}
	
	@FXML
	private void handleBtnZgrade() {
    handleAnyButton(btnZgrade,"../tables/Buildings.fxml","Zgrade");
	}
	
	@FXML
	private void handleBtnCasovi() {
	handleAnyButton(btnCasovi,"../tables/Casovi.fxml", "Casovi");
	}
	
	@FXML
	private void handleBtnUcionice() {
	handleAnyButton(btnUcionice,"../tables/Classrooms.fxml", "Ucionice");
	}

	@FXML
	private void handleBtnGrupe() {
    handleAnyButton(btnGrupe,"../tables/Groups.fxml","Grupe");
	}
	
	@FXML
	private void handleBtnRezervacije() {
	handleAnyButton(btnRezervacije,"../tables/Reservations.fxml", "Rezervacije");
	}
	
	@FXML
	private void handleBtnProfesori() {
	handleAnyButton(btnProfesori,"../tables/Teachers.fxml", "Profesori");
	}
	
	@FXML
	private void handleBtnUsmjerenja() {
	handleAnyButton(btnUsmjerenja,"../MenuUsmjerenja.fxml", "Usmjerenja");
	}
	
	@FXML
	private void handleBtnPredmeti() {
	handleAnyButton(btnPredmeti,"../tables/Subjects.fxml", "Predmeti");
	}
}
