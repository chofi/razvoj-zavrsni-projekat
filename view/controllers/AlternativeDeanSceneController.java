package view.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import view.dialogs.controllers.EditCasController;
import view.dialogs.controllers.EditReservationController;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import api.Api;
import entities.*;

public class AlternativeDeanSceneController extends AlternativeProfessorSceneController {

	@FXML
	public Button btnUnos;

	private void editCas(Cas c) {
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnUnos.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../dialogs/editCas.fxml"));
	        Parent root = loader.load();
	   	    EditCasController editCasController = loader.getController();
	        editCasController.setCas(c);
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Uredi cas");
	        dialog.show();
	        dialog.setOnCloseRequest(event -> { rerender();});
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
	}
	private void makeClickHandlerCas(Node n, Cas c) {
		n.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
			ContextMenu menu = new ContextMenu();
			menu.setAutoHide(true);

			MenuItem edit = new MenuItem("Uredi");
			edit.setOnAction(e ->{
				editCas(c);
			});

			MenuItem del = new MenuItem("Izbrisi");
			del.setOnAction(e ->{
				showConfirmDialog("Da li ste sigurni da zelite izbrisati cas?",
					bool -> {
						if(bool) {
							Api.deleteCas(c);
						}
					});
			});

			MenuItem closeMenu = new MenuItem("Zatvori");

			menu.getItems().add(edit);
			menu.getItems().add(del);
			menu.getItems().add(closeMenu);
			menu.show(raspored, event.getScreenX(), event.getScreenY());
			
		});
	}
	@FXML
	private void handleUnos() {
		
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnUnos.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../MenuUredjivanje.fxml"));
	        Parent root = loader.load();
//	        IzvjestajController c = loader.getController();
//	        c.generate(loggedInUser.getTeacher(), datePicker.getValue());
	        Scene scene=new Scene(root);
	        scene.getStylesheets().add("view/stylesheet.css");
	        dialog.setScene(scene);
	        dialog.setTitle("Uredjivanje");
	        dialog.setOnCloseRequest(event -> {rerender();});     
	        dialog.show();
		}
		catch(Throwable e) {System.out.println(e);}
	}
	@Override
	public void renderCasovi(Collection<Cas> casovi) {
		// first clear old values
		clearHBoxes();

		// render new ones
		for(Cas cas : casovi)
		{
			int dayAsNum = dayToInt(cas.getDay());
			Vector<HBox[]> cellsOfDay = dani.elementAt(dayAsNum);
			HBox hboxOfDay = hboxes[dayAsNum];

			
			HBox[] cells = null;
			for(HBox[] c : cellsOfDay) {
				if(canFit(cas, c)) {
					cells = c;
					break;
				}
			}
			if(cells == null) {
				addNewGrid(cellsOfDay, hboxOfDay);
				cells = cellsOfDay.lastElement();
			}

			VBox vbox = new VBox();


			vbox.getStyleClass().add("top");
			switch(cas.getGroup().getType()) {
			case PRED:
				vbox.getStyleClass().add("pred");
				break;
			case AV:
				vbox.getStyleClass().add("av");
				break;
			case LV:
				vbox.getStyleClass().add("lv");
				break;
			}

			Label lbl = new Label();
			lbl.setText(cas.getGroup().getSubject().getName());
			lbl.setAlignment(Pos.TOP_CENTER);
			lbl.setWrapText(true);
			lbl.setFont(new Font(11));
			lbl.setPrefWidth(10000);
			Label lbl2 = new Label();
			lbl2.setText(cas.getClassroom().getName());
			lbl2.setFont(new Font(11));
			lbl2.setAlignment(Pos.TOP_CENTER);
			lbl2.setPrefWidth(10000);
			lbl2.setWrapText(true);


			for(int i = 1; i < cas.getDuration(); i++) {
				VBox box = new VBox();
				Label label = new Label("");
				label.setPrefWidth(1000);
				label.setPrefHeight(1000);
				box.getChildren().add(label);
				if(i != cas.getDuration() - 1) {
					try {
						box.getStyleClass().add("mid");
					}catch(Throwable e) {e.printStackTrace();}
					
				}else {
					try {
						box.getStyleClass().add("bot");
					}catch(Throwable e) {e.printStackTrace();}
				}
				switch(cas.getGroup().getType()) {
				case PRED:
					box.getStyleClass().add("pred");
					break;
				case AV:
					box.getStyleClass().add("av");
					break;
				case LV:
					box.getStyleClass().add("lv");
					break;
				}
				makeClickHandlerCas(box, cas);
				cells[cas.getStartHour() - 8 + i].getChildren().add(box);
			}

			vbox.getChildren().add(lbl);
			vbox.getChildren().add(lbl2);
			makeClickHandlerCas(vbox, cas);
			cells[cas.getStartHour() - 8].getChildren().addAll(vbox);
		}
		
		disableScrollbars();
	}
}
