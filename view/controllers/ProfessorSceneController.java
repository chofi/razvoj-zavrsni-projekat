package view.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import view.dialogs.controllers.EditReservationController;
import javafx.scene.paint.Color;

import api.Api;
import entities.*;

public class ProfessorSceneController extends StudentSceneController {
	private User loggedInUser = null;

	public User getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(User loggedInUser) {
		lblUsername.setText(loggedInUser.getTeacher().toString());
		this.loggedInUser = loggedInUser;
	}
	@FXML
	public CheckBox toggleShowReservations;
	@FXML
	public Button btnReservation;
	@FXML
	public Button btnIzvjestaj;
	@FXML
	public Label lblUsername;
	@FXML
	public void handleToggleShowReservations() {
		rerender();
	}
	@FXML
	private void handleButtonIzvjestaj() {
		try {
			Stage dialog = new Stage();
			dialog.initModality(Modality.NONE);
			Stage appStage=(Stage)btnIzvjestaj.getScene().getWindow();
			dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../Izvjestaj.fxml"));
	        Parent root = loader.load();
	        IzvjestajController c = loader.getController();
	        c.generate(loggedInUser.getTeacher(), datePicker.getValue());
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Izvjestaj");
	        dialog.setOnCloseRequest(event -> {rerender();});     
	        dialog.show();
		}
		catch(Throwable e) {System.out.println(e);}
	}
	@FXML
	private void handleButtonReservation(ActionEvent e) {
		editReservation(null);
	}

	@FXML
	public void initialize() {
		initStudentController();
		initProfessorController();
	}
	public void initProfessorController() {
		Collection<Reservation> rezervacije = Api.getReservationsInWeekOfDate(datePicker.getValue());
		renderReservations(rezervacije);
	}

	@FXML
	private void handleDatePicker(ActionEvent event) {
			setDates(datePicker.getValue());
			rerender();
	}
	public void rerender() {
		System.out.println("rerender");
		handleFilterProfessor(new ActionEvent(new ChoiceBox<Object>(), new ChoiceBox<Object>()));
	}
	@FXML
	public void handleFilterProfessor(ActionEvent event) {
		handleFilterStudent(event);
		
		Collection<Reservation> rezervacije = Api.getReservationsInWeekOfDate(datePicker.getValue());
		System.out.println("REZERVACIJE");
		for(Reservation r : rezervacije)
		{
			System.out.println(r.getId());
			System.out.println(r.getDescription());
		}
		if(filterBuilding.getValue() != null) {
			rezervacije = Api.filterReservationByBuilding(rezervacije, filterBuilding.getValue());
		}
		
		if(filterClassroom.getValue() != null) {
			rezervacije = Api.filterReservationByClassroom(rezervacije, filterClassroom.getValue());
		}

		if(filterProfessor.getValue() != null) {
			rezervacije = Api.filterReservationByProfessor(rezervacije, filterProfessor.getValue());
		}

		if(filterSubject.getValue() != null) {
			rezervacije = Api.filterReservationBySubject(rezervacije, filterSubject.getValue());
		}

		if(filterSemester.getValue() != null) {
			rezervacije = Api.filterReservationBySemester(rezervacije, filterSemester.getValue());
		}
		if(filterYear.getValue() != null) {
			rezervacije = Api.filterReservationByYear(rezervacije, filterYear.getValue());
		}

		if(filterGroup.getValue() != null) {
			rezervacije = Api.filterReservationByGroup(rezervacije, stringToGroup(filterGroup.getValue()));
		}
		
		
		renderReservations(rezervacije);
	}
	private Boolean reservationBelongToUser(Reservation r) {
		if(loggedInUser.getType() == User.AccountType.Dean)
			return true;

		if(r.getTeacher().getId() == loggedInUser.getTeacher().getId())
			return true;
		else
			return false;
	}
	private void editReservation(Reservation r) {
		if(r != null && !reservationBelongToUser(r)) {
				System.out.println("Error, nije tvoja rezervacija");
				return;
		}
try {
		Stage dialog = new Stage();
		dialog.initModality(Modality.NONE);
		Stage appStage=(Stage)btnLogout.getScene().getWindow();
		dialog.initOwner(appStage);
	
		    FXMLLoader loader = new FXMLLoader(getClass().getResource("../" + "dialogs/editReservation" + ".fxml"));
	        Parent root = loader.load();
	        EditReservationController c = loader.getController();
	        c.setReservation(r);
	        c.setUser(loggedInUser);
	        Scene scene=new Scene(root);
	        dialog.setScene(scene);
	        dialog.setTitle("Rezervacija");
	        dialog.setOnCloseRequest(event -> {rerender();});     
	        dialog.show();
}
catch(Throwable e) {System.out.println(e);}
	}
	private void deleteReservation(Reservation r) {
		if(reservationBelongToUser(r)) {
			System.out.println("Brisem rezervaciju " + r.getId());
			Api.deleteReservation(r);
			rerender();
		}else {
			System.out.println("Error, nije tvoja rezervacija");
		}
	}
	private void displayWarning(MouseEvent event, String errorText) {
		Popup popup = new Popup();
		Label lbl = new Label(errorText);
		lbl.setMinWidth(100);
		lbl.setMinHeight(20);
		lbl.getStyleClass().add("notification");
		lbl.setTextFill(Color.web("#FFFFFF"));
		popup.getContent().add(lbl);
		Stage stage = (Stage) btnReservation.getScene().getWindow();
		popup.setX(stage.getX() + event.getSceneX());
		popup.setY(stage.getY() + event.getSceneY());
		Timeline timeline = new Timeline(new KeyFrame(
				Duration.millis(1000),
				ae -> popup.hide()));
		popup.show(stage);
		timeline.play();
	}
	private void makeClickHandler(Node n, Reservation r) {
		n.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
			ContextMenu menu = new ContextMenu();
			menu.setAutoHide(true);
			MenuItem edit = new MenuItem("Uredi");
			edit.setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent e) {
					editReservation(r);
					if(!reservationBelongToUser(r)) {
						displayWarning(event, "Rezervacija ne pripada vama!");
					}
				}
			});

			MenuItem del = new MenuItem("Izbrisi");
			del.setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent e) {
					deleteReservation(r);
					if(!reservationBelongToUser(r)) {
						displayWarning(event, "Rezervacija ne pripada vama!");
					}
				}
			});

			menu.getItems().add(edit);
			menu.getItems().add(del);
			menu.show(raspored, event.getScreenX(), event.getScreenY());
			
		});
	}
	private void renderReservations(Collection<Reservation> casovi) {
		if(toggleShowReservations.isSelected() == false)
			return;
		// student controller already cleared old hboxes
		for(Reservation cas : casovi)
		{
			VBox vbox = new VBox();
			vbox.getStyleClass().add("vbox");
			vbox.getStyleClass().add("reservation");

			vbox.setAlignment(Pos.TOP_CENTER);
			vbox.setPrefWidth(1000);
			Label lbl = new Label();
			lbl.setText(cas.getTeacher().getName());
			lbl.setAlignment(Pos.CENTER);
			lbl.setPrefWidth(1000);
			Label lbl2 = new Label();
			lbl2.setText(cas.getClassroom().getName());
			lbl2.setAlignment(Pos.CENTER);
			lbl2.setPrefWidth(1000);
			Label lbl3 = new Label();
			lbl3.setText(cas.getDescription());
			lbl3.setAlignment(Pos.CENTER);
			lbl3.setPrefWidth(1000);

			for(int i = 1; i < cas.getDuration(); i++) {
				VBox vbox1 = new VBox();
				vbox1.getStyleClass().add("vbox");
				vbox1.getStyleClass().add("reservation");
				
				vbox1.setAlignment(Pos.TOP_CENTER);
				vbox1.setPrefWidth(1000);

				makeClickHandler(vbox1, cas);

				cells[cas.getDate().getDayOfWeek().getValue()][cas.getStartHour() - 8 + i].getChildren().add(vbox1);
			}

			vbox.getChildren().add(lbl);
			vbox.getChildren().add(lbl2);
			vbox.getChildren().add(lbl3);

			makeClickHandler(lbl, cas);

			cells[cas.getDate().getDayOfWeek().getValue()][cas.getStartHour() - 8].getChildren().addAll(vbox);

		}
	}

}
