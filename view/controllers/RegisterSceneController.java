package view.controllers;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import api.*;
import entities.User;
import entities.User.AccountType;
import entities.Teacher;

public class RegisterSceneController implements Initializable {
	@FXML
	public Button btnRegister;
	@FXML
	public TextField inputUsername;
	@FXML
	public PasswordField inputPassword;
	@FXML
	public TextField inputName;
	@FXML
	public TextField inputSurname;
	@FXML
	public Label lblNotify;
	@FXML
	public ChoiceBox<String> dropdownTypes;
	

	
	@FXML
	private void handleRegister(ActionEvent event) 
	{
	    String username = inputUsername.getText();
	    String password = inputPassword.getText();
	    String name = inputName.getText();
	    String surname = inputSurname.getText();
	 
	    if(	
		        name.equals("") ||
		        username.equals("") ||
		        surname.equals("") ||
		        password.equals("") || 
                dropdownTypes.getValue() == null
				) {
				lblNotify.setText("Ispunite sva polja!");
				return;
			}
	    
	    if(password.length() < 6) {
	    	lblNotify.setText("Sifra mora sadrzavati minimalno 6 karaktera!");
			return;
	    }
	    

        User.AccountType type = User.AccountType.Dean;
	    if(dropdownTypes.getValue().equals("Profesor")) 
	    	type = User.AccountType.Professor;

	    
	    Teacher teacher = new Teacher();
	    teacher.setName(name);
	    teacher.setSurname(surname);
	  
       try {   
         if(Api.insertUser(teacher, username, password, type)) 
  	       switchToLoginScene();
  	     else 
  	 	   lblNotify.setText("Korisnik vec postoji u bazi!");
         } catch(Exception e) {
        	 e.printStackTrace();
        	 lblNotify.setText("Neuspjesna registracija!");

         }
	
	
	}
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb){
		 ObservableList<String> types = FXCollections.observableArrayList();	
	     types.add("Dekan");
	     types.add("Profesor");
		 dropdownTypes.setItems(types);
	
	}

	private void switchToLoginScene()
	{
		    Stage appStage=(Stage)btnRegister.getScene().getWindow();
		    
		    appStage.fireEvent(
		    	    new WindowEvent(
		    	        appStage,
		    	        WindowEvent.WINDOW_CLOSE_REQUEST
		    	    )
		    	);

   }
}
		    
	    
		    