package entities;

import java.io.Serializable;
import javax.persistence.*;
import static javax.persistence.CascadeType.*;

@Entity
@Table(name="CLASSROOMS")
public class Classroom implements Serializable
{
	@Id
	@TableGenerator(name = "ClassroomGenerator")
	@GeneratedValue(generator = "ClassroomGenerator")
	@Column(name = "CLASSROOM_ID")
	private int id;
	private String name;

	@ManyToOne
	@JoinColumn(name = "BUILDING_ID")
	private Building building;


	public int getId() {
		return id;
	}
    
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}
	
	public String toString() {
		return name;
	}
}
