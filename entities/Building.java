package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import javax.persistence.*;

@Entity
@Table(name="BUILDINGS")
public class Building implements Serializable
{
	@Id
	@TableGenerator(name = "BuildingGenerator")
	@GeneratedValue(generator = "BuildingGenerator")
	@Column(name = "BUILDING_ID")
	private int id;
    private String name;
    
   
    
	public int getId() {
		return id;
	}
    
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public String toString() {
    	return name;
    }
}
