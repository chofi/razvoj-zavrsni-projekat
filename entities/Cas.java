package entities;
import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@Entity
@Table(name="CASOVI")
public class Cas implements Serializable{
	public enum Day{
		PON,
		UTO,
		SRI,
		CET,
		PET,
		SUB
	}
	// ovdje bi trebalo kompozitni kljuc napravit vjerovatno
	// al lakse je bubnut id mensecini
	@Id
	@TableGenerator(name = "CasoviGenerator")
	@GeneratedValue(generator = "CasoviGenerator")
	@Column(name = "CAS_ID")
	private int id;
	private Group group;
	private Classroom classroom;
	public Classroom getClassroom() {
		return classroom;
	}
	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	private Day day;
	private int startHour;
	public int getStartHour() {
		return startHour;
	}
	public int getId() {
		return id;
	}
	public void setId(int _id) {
		this.id = _id;
	}
	
	public void setStartHour(int startHour) {
		this.startHour = startHour;
	}
	private int duration;
	
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Day getDay() {
		return day;
	}
	
	public void setDay(Day day) {
		this.day = day;
	}
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public static ObservableList<Day> getDays() {
	
        ObservableList<Day> days = FXCollections.observableArrayList();	
        days.add(Day.PON);
        days.add(Day.UTO);
        days.add(Day.SRI);
        days.add(Day.CET);
        days.add(Day.PET);
        days.add(Day.SUB);
        
        return days;

	}
 
}
