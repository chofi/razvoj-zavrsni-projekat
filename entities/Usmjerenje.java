package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Vector;

import javax.persistence.*;

import javafx.collections.FXCollections;

@Entity
@Table(name="SUBJECTS")
public class Usmjerenje implements Serializable
{

	@Id
	@TableGenerator(name = "UsmjerenjeGenerator")
	@GeneratedValue(generator = "UsmjerenjeGenerator")
	@Column(name="USMJERENJE_ID")
	private int id;
    private String name;
    private Collection<Subject> subjects = new Vector<Subject>();

    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void addSubject(Subject s) {
    	subjects.add(s);
    }
    
    public Collection<Subject> getSubjects() {
    	return subjects;
    }


}
