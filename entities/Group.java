package entities;

import java.io.Serializable;
import javax.persistence.*;

import entities.Cas.Day;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@Entity
@Table(name="GROUPS")
public class Group implements Serializable
{
	public enum GroupType {
		AV,
		LV,
		PRED
	}
	@Id
	@TableGenerator(name = "GroupGenerator")
	@GeneratedValue(generator = "GroupGenerator")
	@Column(name = "GROUP_ID")
	private int id;
	private GroupType type;
	private Teacher teacher;
	private Subject subject;
	private String semester;

	
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public GroupType getType() {
		return type;
	}
	public String getName() {
		return type.toString();
	}
	public void setType(GroupType type) {
		this.type = type;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public Subject getSubject() {
		return subject;
	}
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
    public String toString() {
    	return semester.toString() + "|" + type + ": " + teacher.toString() + ", " + subject.toString();
    }
	public static ObservableList<GroupType> getGroupTypes() {
        ObservableList<GroupType> types = FXCollections.observableArrayList();	
        types.add(GroupType.AV);
        types.add(GroupType.LV);
        types.add(GroupType.PRED);

        return types;

	}
	
}
