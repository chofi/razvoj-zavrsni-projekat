package entities;

import java.time.LocalDate;
import javax.persistence.*;

@Entity
@Table(name="RESERVATIONS")
public class Reservation {
	@Id
	@TableGenerator(name = "ReservationGenerator")
	@GeneratedValue(generator = "ReservationGenerator")
	@Column(name = "RESERVATION_ID")
	private int id;
	private Teacher teacher;
	public int getStartHour() {
		return startHour;
	}
	public void setStartHour(int startHour) {
		this.startHour = startHour;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	private int startHour;
	private int duration;
	private LocalDate date;
	private Classroom classroom;
	private String description;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Classroom getClassroom() {
		return classroom;
	}
	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	public String getDescription() {
		return description;
	}
	public void setDecription(String decription) {
		this.description = decription;
	}
}
